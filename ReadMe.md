# Portainer Update Bot
![Rust](https://img.shields.io/badge/Language-Rust-orange?style=flat)
![License MIT](https://img.shields.io/gitlab/license/MichaelSchmitz/portainer_update_bot?style=flat)
![Telegram](https://img.shields.io/badge/Bot-Telegram-blue?style=flat)

This project provides a [Telegram bot](https://core.telegram.org/bots/api), to interact with the [Portainer](https://www.portainer.io/) api to update Stacks via commands.
Supported stack types are git or local (webhook) based ones.
It provides an inline search experience to update stacks easily.
You have to whitelist any group or chat you wish to use with the bot, avoiding unauthorized updates of your stacks.

## Configuration
All configuration is done inside the `config.toml` file.

Per default the file should be located next to the executable.
If you wish to modify this behaviour, an alternative file location can be set with the environment variable `CONFIG_FILE`.

1. Create a Telegram bot with @BotFather.
2. Configure the commands with @BotFather via:
   1. `/mybots`
   2. Select the new bot
   3. Select `Edit bot`
   4. Select `Edit commands`
   5. Reply with 
      ```
      info - Responds with info required to setup this bot
      help - Display available commands
      update - Update a stack in given environment
      upgrade - Update all stacks in given environment
      ```
   6. Go back to your Bot edit page
   7. Select `Bot Settings`
   8. Select `Inline Mode` and turn it on, you can configure any text to be shown when using `@<your_bot_name>` later on
3. Configure the `config.toml`
   1. Set the bot token
   2. Configure the base url of your portainer instance (must be reachable from the bot instance)
   3. Configure all necessary environments and stacks with their respective webhooks or stack id (for git stacks)
   4. Configure the chats you require the bot to work in, for ease of use you can omit these on the first run and issue a `/info` command.
      The bot will respond with setup instructions including the necessary chat id.

## Usage
Available commands are:
- `/info` Provides setup instructions
- `/help` Overview over commands
- `/update <env> <stack>` Updates given stack on env
- `/upgrade <env>` Updates all configured stacks on env
- `@<your_bot_name> <search>` Search all available stacks, click on a result to execute update

## Docker
This bot can be hosted using docker (compose).
```yaml
services:
  portainer_update_bot:
    container_name: portainer_update_bot
    image: bl4d3s/portainer_update_bot
    volumes:
      - /path/to/your/config.toml:/config.toml:ro
    networks:
      docker-local:

networks:
  docker-local:
    name: docker-local
    external: true
```

