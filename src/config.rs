use std::collections::HashMap;
use config_file::FromConfigFile;
use std::env;
use std::error::Error;
use once_cell::sync::OnceCell;
use serde::{Deserialize, Serialize};
use teloxide::types::ChatId;

type Environments = HashMap<String, HashMap<String, WebHookOrGitOps>>;

#[derive(Debug, Clone, Deserialize)]
#[serde(untagged)]
pub enum WebHookOrGitOps {
    GitOps(i64),
    Webhook(String)
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct GitStack {
    #[serde(rename(serialize = "prune"), skip_serializing_if = "Option::is_none")]
    pub prune_image: Option<bool>,
    #[serde(rename(serialize = "pullImage"), skip_serializing_if = "Option::is_none")]
    pub pull_image: Option<bool>,
    #[serde(rename(serialize = "repositoryAuthentication"), skip_serializing_if = "Option::is_none")]
    pub authentication_required: Option<bool>,
    #[serde(rename(serialize = "repositoryGitCredentialID"), skip_serializing_if = "Option::is_none")]
    pub credential_id: Option<i64>,
    #[serde(rename(serialize = "repositoryPassword"), skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,
    #[serde(rename(serialize = "repositoryReferenceName"), default = "default_reference")]
    pub reference: String,
    #[serde(rename(serialize = "repositoryUsername"), skip_serializing_if = "Option::is_none")]
    pub username: Option<String>,
    #[serde(rename(serialize = "stackName"), skip_serializing_if = "Option::is_none")]
    pub stack_name: Option<String>,
    #[serde(skip_serializing)]
    pub api_key: String,
    #[serde(rename(serialize = "env"), skip_deserializing)]
    pub env: Vec<GitStackEnv>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GitStackEnv {
    #[serde(rename = "name")]
    pub name: String,
    #[serde(rename = "value")]
    pub value: String
}

#[derive(Debug, Clone, Deserialize)]
pub struct Config {
    pub environments: Environments,
    pub allowed_chats: Vec<i64>,
    pub telegram_token: String,
    pub portainer_url: String,
    pub git_stack: Option<GitStack>
}

fn default_reference() -> String {
    "refs/heads/main".to_string()
}

impl Config {
    pub fn global() -> &'static Config {
        INSTANCE.get().expect("Config not loaded")
    }

    pub fn load_from_file() {
        let config_file_name = env::var("CONFIG_FILE").unwrap_or("config.toml".to_string());
        let config = Config::from_config_file(config_file_name).unwrap_or_else(|e| panic!("Wrong configuration syntax: {}\n{:?}", e, e.source().unwrap().to_string()));

        if config.telegram_token.is_empty() {
            panic!("No telegram token set");
        }
        if config.portainer_url.is_empty() {
            panic!("No portainer base url set");
        }
        if config.environments.iter().count() == 0 {
            panic!("No environments configured");
        }

        INSTANCE.set(config).unwrap()
    }

    pub fn is_allowed_chat(&self, id: ChatId) -> bool {
        self.allowed_chats.contains(&id.0)
    }
}

static INSTANCE: OnceCell<Config> = OnceCell::new();