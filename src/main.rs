use teloxide::{prelude::*, RequestError, utils::command::BotCommands};
use teloxide::types::{InlineQueryResult, InlineQueryResultArticle, InputMessageContent, InputMessageContentText, MessageId};
use crate::config::Config;
use crate::update::{trigger_call, get_url, get_stacks, UrlWithMethod};

mod update;
mod config;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    Config::load_from_file();
    log::info!("Starting command bot...");

    let token = Config::global().telegram_token.to_string();
    let bot = Bot::new(token);
    let command_handler = Update::filter_message().filter_command::<PortainerCommands>().endpoint(answer);
    let inline_handler = Update::filter_inline_query().endpoint(inline_answer);

    let handler = dptree::entry()
        .branch(command_handler)
        .branch(inline_handler);

    Dispatcher::builder(bot, handler)
        .enable_ctrlc_handler()
        .build()
        .dispatch()
        .await;
}

#[derive(BotCommands, Clone)]
#[command(rename_rule = "lowercase", description = "These commands are supported:")]
enum PortainerCommands {
    #[command(description = "Responds with info required to setup this bot.")]
    Info,
    #[command(description = "display this text.")]
    Help,
    #[command(description = "update a stack. Parameters: env,stack", parse_with = "split")]
    Update { env: String, stack: String },
    #[command(description = "update all stacks. Parameters: env")]
    Upgrade(String),
}

async fn answer(bot: Bot, msg: Message, cmd: PortainerCommands) -> ResponseResult<()> {
    match cmd {
        PortainerCommands::Info =>
            _ = if Config::global().is_allowed_chat(msg.chat.id) {
                 bot.send_message(msg.chat.id, "Your bot is correctly configured to answer to this chat and execute given updates".to_string()).await?
             } else {
                 bot.send_message(msg.chat.id, format!("Please configure your bot to allow processing of your chat.\nAdd the chat id to the list of allowed ones in the config.toml.\n\nYour chat id: {}", msg.chat.id)).await?
             },
        PortainerCommands::Help => _ = bot.send_message(msg.chat.id, PortainerCommands::descriptions().to_string()).await?,
        PortainerCommands::Update { env, stack } => {
            log::info!("Received update trigger for {} on {}", stack, env);
            if Config::global().is_allowed_chat(msg.chat.id) {
                let url_with_method = get_url(env.to_string(), stack.to_string());

                match url_with_method {
                    None => {
                        log::warn!("Failed to update {} on {}", stack, env);
                        _ = bot.send_message(msg.chat.id, format!("No stack named {stack} is configured on environment {env}.")).await?
                    }
                    Some(url_with_method) => {
                        log::info!("Triggered update for {} on {}", stack, env);
                        let bot_msg = bot.send_message(msg.chat.id, format!("Triggered update for {stack} on environment {env}.")).await?;
                        _ = tokio::spawn(async move {
                            update_message(bot.clone(), msg.chat.id, bot_msg.id, url_with_method, stack.to_string(), env.to_string()).await
                        })
                    }
                }
            } else {
                log::warn!("Unconfigured chat update");

                _ = bot.send_message(msg.chat.id, "Update not allowed from this chat. Please use /info to get instructions to set up this bot.".to_string()).await?
            }
        },
        PortainerCommands::Upgrade(env) => {
            log::info!("Received upgrade trigger on {}", env);
            if Config::global().is_allowed_chat(msg.chat.id) {
                let stacks = get_stacks(env.to_string());

                match stacks {
                    None => {
                        log::warn!("Failed to upgrade {}", env);
                        _ = bot.send_message(msg.chat.id, format!("No environment {env} is configured.")).await?
                    }
                    Some(stacks) => {
                        let bot_msg = bot.send_message(msg.chat.id, format!("Triggered updates on environment {env}")).await?;
                        _ = tokio::spawn(async move {
                            update_all_message(bot.clone(), msg.chat.id, bot_msg.id, stacks, env.to_string()).await
                        })
                    }
                }
            } else {
                log::warn!("Chat is not configured for updates");

                _ = bot.send_message(msg.chat.id, "Update not allowed from this chat. Please use /info to get instructions to set up this bot.".to_string()).await?
            }
        }
    };
    Ok(())
}

async fn update_message(bot: Bot, chat_id: ChatId, msg_id: MessageId, url_with_method: UrlWithMethod, stack: String, env: String) {
    let success = trigger_call(url_with_method).await;
    if success.is_ok_and(|val| val) {
        bot.edit_message_text(chat_id, msg_id, format!("Updated {stack} on environment {env}.")).await.expect("Could not update message");
    }
    else {
        bot.edit_message_text(chat_id, msg_id, format!("Failed to update {stack} on environment {env}.")).await.expect("Could not update message");
    }
}

async fn update_all_message(bot: Bot, chat_id: ChatId, msg_id: MessageId, stacks: Vec<(String, UrlWithMethod)>, env: String) {
    let mut msg_content = format!("Updated environment {env}:").to_string();
    for (name, url_with_method) in stacks {
        log::info!("Triggered update for {name} on {env}");
        let success = trigger_call(url_with_method).await;
        msg_content.push_str(format!("\n  - {name} {}", if success.is_ok_and(|val| val) { "updated" } else { "failed to update" }).as_str());
    }
    bot.edit_message_text(chat_id, msg_id, msg_content).await.expect("Could not update message");
}

pub async fn inline_answer(bot: Bot, update: InlineQuery) -> Result<(), RequestError> {
    let mut inline_result = get_configured_stacks(update.query.as_str());

    let offset = update.offset.parse::<usize>().unwrap_or_default();
    if inline_result.is_empty() {
        inline_result.push(InlineQueryResult::Article(InlineQueryResultArticle::new(
            "1",
            "No stacks configured",
            InputMessageContent::Text(InputMessageContentText::new("No stacks configured")),
        )))
    }
    let mut req_builder = bot.answer_inline_query(update.id, inline_result);
    if offset != 0 {
        req_builder = req_builder.next_offset(offset.to_string());
    }
    req_builder.await?;

    respond(())
}

fn get_configured_stacks(search: &str) -> Vec<InlineQueryResult> {
    let keywords: Vec<&str> = search.split(' ').collect();
    let mut results: Vec<InlineQueryResult> = Vec::new();
    for (environment, stacks) in &Config::global().environments {
        for key in stacks.keys() {
            if keywords.iter().any(|k| key.contains(k)) {
                results.push(InlineQueryResult::Article(InlineQueryResultArticle::new(
                    format!("{}_{}", environment, key),
                    format!("Environment: {}, Stack: {}", environment, key),
                    InputMessageContent::Text(InputMessageContentText::new(format!("/update {} {}", environment, key))),
                )))
            }
        }
    }

    results
}
