use reqwest::{Body, Error, Method, RequestBuilder};
use serde::{Deserialize, Serialize};
use crate::config::{Config, GitStack, GitStackEnv, WebHookOrGitOps};

pub struct UrlWithMethod {
    pub url: String,
    pub method: Method,
    pub git_stack_required: bool
}

pub fn get_url(env: String, stack: String) -> Option<UrlWithMethod> {
    let environment = Config::global().environments.get(&env);

    match environment {
        None => {
            log::warn!("Environment not found: {}", env);
            None
        },
        Some(environment) => {
            let id = environment.get(&stack);
            match id {
                None => {
                    log::warn!("Stack not found: {} on {}", stack, env);
                    None
                },
                Some(id) => {
                    let base_url: String = Config::global().portainer_url.as_str().to_owned();
                    Some(webhook_or_git_ops_to_url(base_url, id))
                }
            }
        }
    }
}

fn webhook_or_git_ops_to_url(base_url: String, id: &WebHookOrGitOps) -> UrlWithMethod {
    match id {
        WebHookOrGitOps::GitOps(git_ops) => UrlWithMethod { url: base_url +  "/api/stacks/" + git_ops.to_string().as_str(), method: Method::PUT, git_stack_required: true },
        WebHookOrGitOps::Webhook(webhook) => UrlWithMethod { url: base_url +  "/api/stacks/webhooks/" + webhook, method: Method::POST, git_stack_required: false }
    }
}

pub fn get_stacks(env: String) -> Option<Vec<(String, UrlWithMethod)>> {
    let environment = Config::global().environments.get(&env);
    let base_url: String = Config::global().portainer_url.as_str().to_owned();

    match environment {
        None => {
            log::warn!("Environment not found: {}", env);
            None
        },
        Some(environment) =>
            Some(environment.iter().map(|(name, id)| (name.to_string(), webhook_or_git_ops_to_url(base_url.clone(), id))).collect())
    }
}

#[derive(Debug, Clone, Deserialize)]
struct StackMeta {
    #[serde(rename(deserialize = "EndpointId"))]
    pub endpoint_id: i64,
    #[serde(rename(deserialize = "Env"))]
    pub env: Vec<GitStackEnv>
}

pub async fn trigger_call(url_with_method: UrlWithMethod) -> Result<bool, Error> {
    let client = reqwest::Client::new();
    let req: RequestBuilder;
    if url_with_method.git_stack_required
    {
        let git_stack = Config::global().git_stack.clone();
        match git_stack {
            None => {
                log::error!("No git stack configuration found, but is required for calling git stack updates");
                return Ok(false)
            }
            Some(mut git_stack) => {
                let prerequisites = client.request(Method::GET, url_with_method.url.as_str()).header("X-API-KEY", git_stack.api_key.as_str()).send().await?.json::<StackMeta>().await;
                if prerequisites.is_err() {
                    log::warn!("Failed querying stack");
                    return Ok(false)
                }
                let meta = prerequisites.unwrap();
                git_stack.env = meta.env;
                req = client.request(url_with_method.method, url_with_method.url + "/git/redeploy?endpointId=" + meta.endpoint_id.to_string().as_str()).header("X-API-KEY", git_stack.api_key.as_str()).json(&git_stack)
            }
        }
    }
    else {
        req = client.request(url_with_method.method, url_with_method.url);
    }
    let resp = req.send().await;
    Ok(resp.ok().is_some_and(|resp| resp.status().is_success()))
}